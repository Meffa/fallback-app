from flask import Flask, render_template, request, redirect, url_for
from werkzeug.utils import secure_filename
import numpy as np
import os


app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'GET':
        return render_template('index.html')

    if request.method == 'POST':
        f = request.files['file']
        f.save(secure_filename(f.filename))
        return redirect(f'/report?f_name={f.filename}')


@app.route('/report')
def solve():

    f_name = request.args.get('f_name')

    try:
        with open(f_name) as f:
            inp = f.readlines()
    except:
        return render_template('report.html', text=['Report is missing'])
    res = []
    for line in inp:
        buff = []
        for feat in line.split(','):
            try:
                buff.append(float(feat.strip()))
            except:
                continue
        res.append(np.mean(buff))
    os.remove(f_name)
    return render_template('report.html', text=res)




if __name__ == '__main__':
    app.run("0.0.0.0")
